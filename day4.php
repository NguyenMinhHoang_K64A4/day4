<!DOCTYPE html>
<html lang="vn">
<head>
    <meta charset="UTF-8">
</head>
    <title>Sign Up</title>
<body>
    
    <fieldset style='width: 500px; height: 450px; margin: auto; margin-top: 20px; boder:#1E90FF solid'>
    <?php
        $name = $gender = $industryCode = $brithOfDate = $address="";
         if($_SERVER["REQUEST_METHOD"]== "POST") {
            if(empty(inputHandling($_POST["name"]))) {
                echo "<div style='color: red;'>Hãy nhập họ tên</div>";
            }
            if (empty($_POST["gender"])) {
                echo "<div style='color: red;'>Hãy chọn giới tính</div>";
            }
            if(empty(inputHandling($_POST["industryCode"]))){
                echo "<div style='color: red;'>Hãy chọn khoa</div>";
            }    
            if(empty(inputHandling($_POST["birthOfDate"]))){
                echo "<div style='color: red;'>Hãy nhập ngày sinh</div>";
            }
            elseif (!validateDate($_POST["birthOfDate"])) {
                echo "<div style='color: red;'>Hãy nhập ngày sinh đúng định dạng</div>";
            }
        }
        function inputHandling($data) {
            $data = trim($data);
            $data = stripslashes($data);
            return $data;
        }
        function validateDate($date){
            if (preg_match("/^[0-9]{4}-(0[1-9]|1[0-2])-(0[1-9]|[1-2][0-9]|3[0-1])$/",$date)) {
                return true;
            } else {
                return false;
            }
        }
        
    ?>
    <form style='margin: 20px 50px 0 30px' method="post">
    <table style = 'border-collapse:separate; border-spacing:15px 15px;'>
            <tr height = '40px'>
                <td width = 30% style = 'background-color: #1e8dcc; 
                vertical-align: top; text-align: center; padding: 5px 5px'>
                    <label style='color: white;'>Họ và tên<span style='color:red;'>*</span></label>
                </td>
                <td width = 30% >
                    <input type='text' name = "name" style = 'line-height: 32px; border-color:#82cd79'>
                </td>
            </tr>
            <tr height = '40px'>
                <td width = 30% style = 'background-color: #1e8dcc; vertical-align: top; text-align: center; padding: 5px 5px'>
                    <label style='color: white;'>Giới tính<span style='color:red;'>*</span></label>
                </td>
                <td width = 30% >
                <?php
                    $gender=array("Nam","Nữ");
                    for($i = 0; $i < count($gender); $i++){
                        echo"
                            <label class='container'>
                                <input type='radio' value=".$gender[$i]." name='gender'>"
                                .$gender[$i]. 
                            "</label>";
                    }
                ?>  

                </td>
            </tr>
            <tr height = '40px'>
                <td style = 'background-color: #2E8BC0; vertical-align: top; text-align: center; padding: 5px 5px'>
                    <label style='color: white;'>Phân Khoa<span style='color:red;'>*</span></label>
                </td>
                <td height = '40px'>
                    <select name='industryCode' style = 'border-color:#82cd79;height: 100%;width: 80%;'>
                        <?php
                            $industryCode=array(""=>"","MAT"=>"Khoa học máy tính","KDL"=>"Khoa học vật liệu");
                            foreach($industryCode as $i=>$i_value){
                                echo"<option>".$i_value."</option>";
                            }
                        ?>
                    </select>
                </td>
            </tr>
            <tr height = '40px'>
                <td style = 'background-color: #2E8BC0; vertical-align: top; text-align: center; padding: 5px 5px'>
                    <label style='color: white;'>Ngày sinh<span style='color:red;'>*</span></label>
                </td>
                <td height = '40px'>
                    <input type='date' name="birthOfDate" data-date="" data-date-format="DD MMMM YYYY" style = 'line-height: 32px; border-color:#82cd79'>
                </td>
            </tr>
            <tr height = '40px'>
                <td style = 'background-color: #2E8BC0; vertical-align: top; text-align: center; padding: 5px 5px'>
                    <label style='color: white;'>Địa chỉ</label>
                </td>
                <td height = '40px'>
                    <input type='text' name="address" style = 'line-height: 32px; border-color:#82cd79'> 
                </td>
            </tr>
        </table>
        <button style='background-color: #49be25; border-radius: 10px; 
        width: 35%; height: 43px; border-width: 0; margin: 20px 130px; color: white;'>Đăng Kí</button>
    </form>
    </fieldset>
</body>
</html>